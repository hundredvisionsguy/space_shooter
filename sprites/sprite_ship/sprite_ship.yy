{
    "id": "1718af20-fb4a-4f3b-92d4-7c6990abf819",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 74,
    "bbox_left": 0,
    "bbox_right": 98,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afa7a43d-b666-4f28-bd88-0d982ecb0ef1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1718af20-fb4a-4f3b-92d4-7c6990abf819",
            "compositeImage": {
                "id": "e74e5802-36a8-440f-ad3c-649c58eb7a30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afa7a43d-b666-4f28-bd88-0d982ecb0ef1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "275923e6-568f-49b4-826b-4c0b84b3699f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afa7a43d-b666-4f28-bd88-0d982ecb0ef1",
                    "LayerId": "fedf12c3-db85-4ac1-8496-c2053fa033fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 75,
    "layers": [
        {
            "id": "fedf12c3-db85-4ac1-8496-c2053fa033fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1718af20-fb4a-4f3b-92d4-7c6990abf819",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 99,
    "xorig": 0,
    "yorig": 0
}